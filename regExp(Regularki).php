﻿<?php

mb_internal_encoding('utf-8');

echo "<h3>Пример 6</h3>";

echo "<h3>Задача на проверку номера.</h3>";

$number = "т180ур";
$regexp = "/^[а-я][0-9]{3}[а-я]{2}/u";

if(preg_match($regexp,$number)){
	echo "Номер {$number} правильный!";
}else{
	echo "Номер {$number} не правильный!";
}

echo "<h3>Задача на проверку телефонов.</h3>";

$correctNumbers = [ 
  '84951234567',  '+74951234567', '8-495-1-234-567', 
  ' 8 (8122) 56-56-56', '8-911-1234567', '8 (911) 12 345 67', 
  '8-911 12 345 67', '8 (911) - 123 - 45 - 67', '+ 7 999 123 4567', 
  '8 ( 999 ) 1234567', '8 999 123 4567'
];
$incorrectNumbers = [
  '02', '84951234567 позвать люсю', '849512345', '849512345678', 
  '8 (409) 123-123-123', '7900123467', '5005005001', '8888-8888-88',
  '84951a234567', '8495123456a', 
  '+1 234 5678901', /* неверный код страны */
  '+8 234 5678901', /* либо 8 либо +7 */
  '7 234 5678901' /* нет + */
];
$regexp_ = "/^(\s*\+\s*7|\s*8)([-\s()]*[\d+][-\s()]*){10}$/";

$countTryNumb = null;
$countFalseNumb = null;

echo "<table>";

for($i=0;$i<count($correctNumbers);$i++){
	if(preg_match($regexp_,$correctNumbers[$i])){
		echo "<tr><td>{$correctNumbers[$i]}</td><td>правильный номер!</td></tr>";
		$countTryNumb ++;
	}else{
		echo "<tr><td>{$correctNumbers[$i]}</td><td>неправильный номер!</td></tr>";
		$countFalseNumb++;
	}
}



for($i=0;$i<count($incorrectNumbers);$i++){
	if(preg_match($regexp_,$incorrectNumbers[$i])){
		echo "<tr><td>{$correctNumbers[$i]}</td><td>правильный номер</td></tr>";
		$countTryNumb ++;
	}else{
		echo "<tr><td>{$incorrectNumbers[$i]}</td><td>неправильный номер!</td></tr>";
		$countFalseNumb++;
	}
}

echo "</table>";
echo '<p>'.$countTryNumb.' правельных номеров!</p>';
echo '<p>'.$countFalseNumb.' неправельных номеров!</p>';

echo "<h3>Привести номер к единному формату!</h3>";

$numberMass = ['8-911-404-44-11','+7(812)6786767'];

$regexp__ = "/(^\s*[8]*|\s*^[\+7]*)*([-\s()]*)/";

for($i=0;$i<count($numberMass);$i++){
	echo '<p>'.$numberMass[$i].'</p>';
}
for($i=0;$i<count($numberMass);$i++){
	if(preg_match($regexp_,$numberMass[$i])){
	$result = preg_replace($regexp__,'',$numberMass[$i]).'<br>';
	echo '8'.$result;
	}
}

echo "<h3>Автозамена</h3>";

$text = 'ты такой d ur a k, дурак, ДуРаК, DyRak!';

$regexpText = "/[дДdD]\s*[уУyYuU]\s*[рРrR]\s*[аАaA]\s*[кКkK]/u";

echo "<p>Строка без замены - {$text}!</p>";

$textAfterReplace = preg_replace($regexpText,'хороший человек',$text);

echo "<p>Строка после замены - {$textAfterReplace}</p>";

echo "<h3>Вывести набор email-лов из тескта</h3>";

$text = 'Я отправил рассылку на mihail56mail@gmail.com,'
        .'mihail56@hotmail.com,'
        .'m-vip56@mail.com!';

$regexpText = "/[a-zA-Z0-9_+.-]+@[a-z.-]+/";				

$emails = array();

preg_match_all($regexpText,$text,$emails);	

$emailsString = implode(", ",$emails[0]);
 

echo "<p>Текст который содержит email адреса: {$text}</p>"
	."<p>Адреса из текста: {$emailsString}!</p>";
	
echo "<h3>Поиск ошибок в тексте</h3>";

$text = "Этим летом я решил заниматься учебой. Я решил координально повысить уровень знаний 
в програмировании. Честно говоря я думал это будет не так трудно но это очень трудно!Прошел 
уже месяц, но я не сильно поднял уровень знаний. Сдесь невозможно учится. всем от тебя постоянно 
что то нужно! Родители не понимают а как бы я хотел чтоб меня оставили в покое!Наверное я не успею 
зделать все что задумал. Слишком жырный план!";

$regExp = "/координально|сдесь|здела[лнюет]{1,2}|\\s+а\\s+|\\s+но\\s+|[,\\.!\\?:;][^\\s]|[a-яё]*жы|шы/iu";

$textArray = array();

$arrayErrors = [        "/координально/ui"        => "- Ошибка в слове \"кардинально\".",
	                    "/сдесь/ui"               => "- Не \"сдесь\", а \"здесь\".",
	                    "/здела[лнюет]{1,2}/ui"     => "- Приставки \"з\" нет в русском языке.",
	                    "/\\s+а\\s+/ui"           => "- Пропущена запятая перед союзом \"а\".",
	                    "/\\s+но\\s+/ui"          => "- Пропущена запятая перед союзом \"но\".",
	                    "/[a-яё]*жы|шы/ui"    => "-\"Жи-Ши\" пиши с \"и\"!",
	                    "/[,\\.!\\?:;][^\\s]/ui"  => "- Пропущен пробел после знака препинания."];


preg_match_all($regExp,$text,$textArray);

echo "<p style='width:500px;'>{$text}</p>"
	."<strong>Ошибки в тексте:</strong>"
	."<table>";

$charArr = array();
	
for($i=0;$i<count($textArray[0]);$i++){
	foreach($arrayErrors as $key => $value){
		if(preg_match($key, $textArray[0][$i])){
			$textPosition = $textArray[0][$i];
			$string = mb_substr($text,mb_stripos($text,$textPosition),50);
			$count = $i + 1;
			echo "<tr><td>{$count}) </td><td>{$string}</td><td>-{$value}</td></td>";
			break;
		}
	}
}

echo "</table>";

echo "<h3>Исправить ошибки в предыдущим тексте</h3>";

$regProbelZnak = '/([\,\;\:\!\?\»])([а-яёА-ЯЁa-z0-9«])/iu';
$regKardinalno = '/координально/iu';
$regSdelal = '/(здела)(ет|ть|л)/iu';
$regJiShi = '/([Жж]|[Шш])ы/iu';
$regProbelNoA = '/([а-яёА-ЯЁa-z0-9]{1,})(\\s+)(а|но)(\\s+)/iu';
 
$text = preg_replace($regProbelZnak, '$1 $2', $text);
$text = preg_replace($regKardinalno, 'кардинально', $text);
$text = preg_replace($regSdelal, 'сдела$2', $text);
$text = preg_replace($regJiShi, '$1и', $text);
$text = preg_replace($regProbelNoA, '$1,$2$3$4', $text);
 
echo "<p style='width:500px;'>{$text}</p>";

echo "<h3>Опечаточник</h3>";

$text = "размещение государственного заказа на право заключение государственного контракта на выпoлнение рабoт по комплекснoму благоустрoйству двoрoвой территoрии по адресу: ул.Гурьянoва д.2 к.2!";

$regExp = "/[\s]/";
$regExpEng = "/^([а-яё]+)([a-z])/ui";
$regExpN = "/[a-z]/ui";
$countWordWithEngChar = 0;

$textArray = array();
$charFixArrayss = array();

 $replacementRus =  ['a' => 'а',
                       'A' => 'А',
                       'B' => 'В',
                       'C' => 'С', 
                       'c' => 'с', 
                       'e' => 'е', 
                       'E' => 'Е', 
                       'H' => 'Н',
                       'K' => 'К',
                       'M' => 'М',
                       'o' => 'о',
                       'O' => 'О',
                       'p' => 'р',
                       'P' => 'Р',
                       'T' => 'Т',
                       'x' => 'х',
                       'X' => 'Х',
                       'y' => 'у'];


$textArray = preg_split($regExp,$text);

echo "<p style='width:500px;'>{$text}</p>";

for($i=0;$i<count($textArray);$i++){
	if(preg_match($regExpEng,$textArray[$i])){
		$countWordWithEngChar++;
		
		$valueFix = preg_replace($regExpEng,'$1[$2]',$textArray[$i]);
	    echo "<p> Опечатка в слове {$textArray[$i]}: {$valueFix}</p>";
		
		preg_match_all($regExpN,$textArray[$i],$charText);
		
		foreach($charText[0] as $letterText){
			foreach($replacementRus as $letterEng => $letterRus){
				if($letterText == $letterEng){						
					$fixWord =  preg_replace("/^([а-яё]+)([a-z])/iu","$1$letterRus",$textArray[$i]);
					$textArray[$i] = $fixWord;
				}
			}
		}	
	}
}

echo "<p><strong>Количество слов с английскими символами: {$countWordWithEngChar}</strong></p>";
$countWordWithEngChar = 0;
	 
$text = implode(' ',$textArray);
echo "<strong>Исправленный текст</strong><p style='width:500px;'>{$text}</p>";
 
$textArray = preg_split($regExp,$text);

for($i=0;$i<count($textArray);$i++){
	if(preg_match($regExpEng,$textArray[$i])){	
	    $countWordWithEngChar++;
		textVerification($regExpEng,$textArray[$i]);
	}
}
echo "<p><strong>Количество слов с английскими символами: {$countWordWithEngChar}</strong></p>";



