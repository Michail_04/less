﻿<?php

//хранит значения ячейки списка.
class StrList{
	
	private $name;
	private $value;
	private $_next;//ссылка на следующаю ячейку списка
	
	
	public function __construct($_name,$_value){
		if($_name != null && $_value!= null){
			$this->name = $_name;
			$this->value = $_value;
			$this->_next = null;//последнее ячейка всегда = null
		}
	}
	//открытый метод для изменения ссылки на ячейку.
	public function get($get){
		$this->_next = $get;
	}
	
}
class _List{
	protected $head; // тут хранится список
	protected $tail; // указатель на последнию ячейку списка
	
	//в начале списк пуст.
	public function __construct(){
		$this->head=null;	
	}
}

//добовления значений в список
class AddInList extends _List{

	public function comp_in($l,$name,$value){
		$data = new StrList($name,$value);//объект с данными 
		if($l->head == null){ 
			$l->head = $data; // если список пустой присваиваем объест с данными в head 
		}else{
			$l->head->get($data); // записываем в следующею ячейку данные
		}
		$l->tail=$data; // данные последней ячейки.
	echo "Добавил в список - имя: ".$name." значение: ".$value."<br>";
	}
}


//тесты
$a = new AddInList;
$s = new _List;
$a->comp_in($s,1,"один");
$a->comp_in($s,2,"два");
$a->comp_in($s,3,"три");
