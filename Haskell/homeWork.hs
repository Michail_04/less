help =  putStrLn("Functions:\n-printTN(x);\n-printFactorial (x);\n-printInvolution (x,y);\n-printSPN (n)")

factorial 1 = 1
factorial n = n*factorial(n-1)

involution x y = [x^a|a<-[1..y]]

trianNumb n = n*(n + 1)/2

squarePyramidalNumber n = ((n*(n+1))*(2*n+1))/6 

printTN x = putStrLn("if x = " ++ show x ++ ", Triangular numbers = " ++ show(trianNumb x))

printFactorial x =  putStrLn("if x = " ++ show x ++ ", Factorial = " ++ show(factorial x))

printInvolution x y = putStrLn("if x = " ++ show x ++ ", y = " ++ show y ++ ", Involution = " ++ show(involution x y))

printSPN x =  putStrLn("if x = " ++ show x ++ ", SquarePyramidalNumber = " ++ show(squarePyramidalNumber x))