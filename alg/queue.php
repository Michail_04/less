﻿<?php

class Queue{
	
	protected $queue;
	protected $limit;
	
	public function __construct($limit = 9){
		$this->queue=array();
		$this->limit = $limit;
	}
	
	public function push($item){
		if(count($this->queue) < $this->limit){
			$this->queue[] = $item;
			echo "Добавил - ". $item."<br>";
		}else{
			echo "Очередь переполнена!";
		}
	}
	
	public function pop(){
		if(empty($this->queue)){
			echo "Стек пуст!";
		}else{
		 echo "Удалил - " .$this->queue[0]."<br>";
			unset($this->queue[0]);
			foreach($this->queue as $key=>$value){
				$key--;
				$this->queue[$key]=$value;
			}
			$count = count($this->queue)-1;
			unset($this->queue[$count]);
		}
	}
	
	public function top() {
		echo "Первое значение - ".$this->queue[0].'<br>';
    }
}

$s = new Queue;
$s->push(1);
$s->push(2);
$s->push(3);
$s->push(4);
$s->push(4);
$s->push(4);
$s->push(4);
$s->push(4);
$s->push(4);
$s->pop();
$s->pop();
$s->top();
var_dump($s);