﻿<?php

// Staring straight up into the sky ... oh my my
error_reporting(-1);
mb_internal_encoding('utf-8');

/* Возвращает соответствующую числу форму слова: 1 рубль, 2 рубля, 5 рублей */
function inclineWord($number, $word1, $word2, $word3) {
	
	if(floor($number / 10)==0){
		if($number % 10 == 1){
			return $word1;
		}
		if($number % 10 > 1 && $number % 10 < 5){
			return $word3;
		}
		if($number % 10 > 4){
			return $word2;
		}
	}
	
	if(floor($number / 10)!=0 && floor($number / 10)<10){
		if($number % 10 == 0 || floor($number / 10) == 1 || 
		floor($number / 10) != 1 && $number % 10 > 4){
			return $word2;
		}
		if(floor($number / 10) != 1 && $number % 10 == 1){
			return $word1;
		}
		if(floor($number / 10) != 1 && $number % 10 > 1 && $number % 10 < 5){
			return $word3;
		}	
	}
	
	if(floor($number / 100)!=0){
		if($number % 100 == 0 || floor($number / 10) == 11 ||
		 floor($number / 10) != 11 && $number % 10 > 4){
			return $word2; 
		}
		if(floor($number / 10) != 11 && $number % 10 == 1){
			return $word1;
		}
		if(floor($number / 10) != 11 && $number % 10 > 1 && $number % 10 < 5){
			return $word3;
		}	 	 
	}
	
	if($number == 0){
		return $word2;
	}
	
}
function numberIntoArray($number){
	$numberArray = array();
	$devRemain = null;
			
		if(floor($number / 100)!=0){
			if($number % 100){
				$devRemain = $number % 100;
				$number -= $devRemain;
				$numberArray[] = $number;
				$number = $devRemain;
			}else{
				$numberArray[] = $number;
			}
		}
		
		if(floor($number / 10)!=0 && floor($number / 10)<10){
			if($number % 10 && floor($number / 10)>1){
				$devRemain = $number % 10;
				$number -= $devRemain;
				$numberArray[] = $number;
				$number = $devRemain;
			}else{
				$numberArray[] = $number;
			}
		}
		
		if(floor($number / 10)==0){
			$numberArray[] = $number;
		}
	return $numberArray;
}

function smallNumberToText($number, $isFemale) {
    $spelling = array(
        0   =>  'ноль',                                     10  =>  'десять',       100 =>  'сто',
        1   =>  'один',         11  =>  'одиннадцать',      20  =>  'двадцать',     200 =>  'двести',
        2   =>  'два',          12  =>  'двенадцать',       30  =>  'тридцать',     300 =>  'триста',
        3   =>  'три',          13  =>  'тринадцать',       40  =>  'сорок',        400 =>  'четыреста',
        4   =>  'четыре',       14  =>  'четырнадцать',     50  =>  'пятьдесят',    500 =>  'пятьсот',
        5   =>  'пять',         15  =>  'пятнадцать',       60  =>  'шестьдесят',   600 =>  'шестьсот',
        6   =>  'шесть',        16  =>  'шестнадцать',      70  =>  'семьдесят',    700 =>  'семьсот',    
        7   =>  'семь',         17  =>  'семнадцать',       80  =>  'восемьдесят',  800 =>  'восемьсот',
        8   =>  'восемь',       18  =>  'восемнадцать',     90  =>  'девяносто',    900 =>  'девятьсот',
        9   =>  'девять',       19  =>  'девятнадцать'    
    );
    
    $femaleSpelling = array(
        1   =>  'одна',        2   =>  'две'
    );
	

	if($isFemale == 0){
		foreach($spelling as $key => $value){
			if($number == $key){
				return $value;
			}
		}
	}else{
		foreach($femaleSpelling as $key => $value){
			if($number == $key){
				return $value;
			}
		}
	}
}

function numberToText($number) {
$saveNumber = $number;
$returnText = null;
$number = number_format($number);
$splitNumbArray = preg_split('/,/',$number);
	
	foreach($splitNumbArray as $key => $value){
		$splitNumbArray[$key] = (int)$value;
		$splitNumbArray[$key] = numberIntoArray($value);
	}
	
$count = count($splitNumbArray);

	foreach($splitNumbArray as $key => $value){
		foreach($splitNumbArray[$key] as $keyNumber => $valueNumber){		
			
			if($count ==3 && $key == 1){
				
				if($valueNumber == 1 || $valueNumber == 2){
					$numb = smallNumberToText($valueNumber, 1);
				}else{
					$numb = smallNumberToText($valueNumber, 0);
				}
				
			}else 		
			if($count ==2 && $key == 0){
				if($valueNumber == 1 || $valueNumber == 2){
					$numb = smallNumberToText($valueNumber, 1);
				}else{
					$numb = smallNumberToText($valueNumber, 0);
				}
			}else{
				$numb = smallNumberToText($valueNumber, 0);
			}
			
			$splitNumbArray[$key][$numb] = $valueNumber;
			unset($splitNumbArray[$key][$keyNumber]);
		}
	}
	
	$splitNumbArray = array_reverse($splitNumbArray);
	
	for($i=0;$i<$count;$i++){
		
	   if($i ==0){
			$sum = array_sum($splitNumbArray[$i]);		   
			$text = inclineWord($sum,'рубль','рублей','рубля');
			$text = " ({$saveNumber}) ".$text.'.';
			$splitNumbArray[$i][$text] = 0;
	   }
	   
	   if($i ==1){
		    $sum = array_sum($splitNumbArray[$i]);
			if(!empty($sum)){
			$text = inclineWord($sum,'тысяча','тысяч','тысячи');
			$splitNumbArray[$i][$text] = 0;}  
	    }
		
	   if($i == 2){
		    $sum = array_sum($splitNumbArray[$i]);
			if(!empty($sum)){
			$text = inclineWord($sum,'миллион','миллионов','миллиона');
			$splitNumbArray[$i][$text] = 0;} 
	   }  
	   
	}
	
	$splitNumbArray = array_reverse($splitNumbArray);
	
	foreach($splitNumbArray as $key => $value){
		foreach($splitNumbArray[$key] as $keyNumber => $number){
			
			if($count ==3 && $keyNumber != 'ноль'){
			$returnText .= $keyNumber.' ';
			}
			
			if($count ==2 && $keyNumber != 'ноль'){
				$returnText .= $keyNumber.' ';
			}
			
			if($count == 1){
				$returnText .= $keyNumber.' ';
			}
			
		}
	}
return $returnText;
}


/* Вызовем функцию несколько раз */

echo "Дана сумма, находящаяся в банке на счету, в рублях. 
Вывести ее в текстовом виде вроде \"шестнадцать миллионов десять 
тысяч три (16010003) рубля\".";


$amount1 = mt_rand(1,99999999);
$text1 = numberToText($amount1);

echo "<p>На вашем счету {$text1}</p>";

$amount2 = mt_rand(1,99999999);
$text2 = numberToText($amount2);

echo "<p>На вашем счету {$text2}</p>";

$amount3 = mt_rand(1,99999999);
$text3 = numberToText($amount3);

echo "<p>На вашем счету {$text3}</p>";

$amount4 = mt_rand(1,99999999);
$text4 = numberToText($amount4);

echo "<p>На вашем счету {$text3}</p>";
