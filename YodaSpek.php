﻿<?php

error_reporting(-1);
mb_internal_encoding('utf-8');

//$text = "Кажется, нас обнаружили! Надо срочно уходить отсюда, пока не поздно. Бежим же скорее!";
// Другие варианты для тестов
//$text = "Ну, прости меня! Не хотела я тебе зла сделать; да в себе не вольна была. Что говорила, что делала, себя не помнила.";
$text = "Идет гражданская война. Космические корабли повстанцев, наносящие удар с тайной базы, одержали первую победу, в схватке со зловещей Галактической Империей.";

/* Делает первую букву предложения заглавной */
function makeFirstletterUppercase($text) {
    return preg_replace_callback('/(([.!?]|^)\s*)(\w)/ui', function($matches) {
	return $matches[1] .' '.mb_strtoupper($matches[3]);
	},$text);
}

function makeYodaStyleText($text) {  
	$array = preg_split("/[.!?]\s*/ui",$text,-1,PREG_SPLIT_NO_EMPTY);
	foreach($array as $key => $value){
		$string = mb_strtolower($value);
		$string = preg_replace("/[,;]/ui","",$string);
		$replaceArray = preg_split("/\s/ui",$string);
		$replaceArray = array_reverse($replaceArray);
		$string = implode(" ",$replaceArray).'.';
		$array[$key] = $string;
	}
	$text = implode('',$array);
	return makeFirstletterUppercase($text);
}



$yodaText = makeYodaStyleText($text);
echo "Йода говорит: {$yodaText}\n";