﻿<?php

error_reporting(-1);

echo "<h3>Пример 4.</h3>";

$rates = array(3, 5, 3, 2, 1, 8, 4, 3, 4, 3, 2, 3);


$ratesSum = array_sum($rates);
$ratesCount = count($rates); 
$averageRate = round($ratesSum/$ratesCount,2);

echo "Анону поставили {$ratesCount} оценок, их сумма = {$ratesSum}<br>Средний балл — {$averageRate}";

echo "<h3>Рост школьников</h3>";

$anonHeight = 169;

echo "<p>Рост школьника {$anonHeight} см.</p>";

$classmates = array(
	'Антон'	=>	172,
	'Семен'	=>	165,
	'Лена'	=>	189,
	'Иван'	=>	171,
	'Петр'	=>	182,
	'Сидор'	=>	176,
	'Аня'	=>	180,
	'Таня'	=>	179,
	'Маня'	=>	171
);

$number = 0; 

foreach ($classmates as $name => $height) {
    echo "Имя: {$name}, рост: {$height} см.<br>";
	if($height > $anonHeight){
		$number++;
	}
}

echo "<p>В классе {$number} человек выше школьника!</p>";

echo "<h3>Рандомный ответ на вопрос.</h3>";

$answers = array('да','нет','не знаю','это зависит от тебя');

$question = 'Выучу ли я PHP?';
$random = array_rand($answers,1);
$answer = $answers[$random];

echo "Вопрос: {$question}<br>Ответ: {$answer}.";

echo "<h3>Генератор имен.</h3>";

$letters = array('ко','и','дзу','ми','са','ку','ра','да','чи','а',
'ки','ми','на','го','ха','ру');

$name = null;

for($i=0;$i<=4;$i++){
	
	$random = array_rand($letters,1);
	$randomText = $letters[$random];
	
	echo "Выпало число {$random}, слог {$randomText}.<br>";
	$name .= $randomText;
}

echo "----------<br>Имя: {$name}";

