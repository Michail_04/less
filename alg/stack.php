﻿<?php

class Stack{
	
	protected $stack;
	protected $limit;
	
	public function __construct($limit = 9){
		$this->stack=array();
		$this->limit = $limit;
	}
		
	public function push($item){
		
		if(count($this->stack) < $this->limit){
			if(!$this->stack){
				$this->stack[] = $item;
			}else{
				foreach($this->stack as $key=>$value){
					$key++;
					$this->stack[$key]=$value;
					$this->stack[0] = $item;
				}
				echo "Добавил - ".$item."<br>";
			}
		}else{
			echo "Стек переполнен!";
		}
	}
	
	public function pop(){
		
		if(empty($this->stack)){
			echo "Стек пуст!";
		}else{
		    echo "Удалил - " .$this->stack[0]."<br>";
			unset($this->stack[0]);
			foreach($this->stack as $key=>$value){
				$key--;
				$this->stack[$key]=$value;
			}
			$count = count($this->stack)-1;
			unset($this->stack[$count]);
		}
	}
	
	 public function top() {
		echo "Первое значение - ".$this->stack[0].'<br>';
    }
}

$a = new Stack;
$a->push(9);
$a->push(7);
$a->push(4);
$a->push(3);
$a->push(2);
$a->push(1);
$a->push(10);
$a->pop();
$a->pop();
$a->pop();
$a->top();
$a->push(4);
$a->top();
$a->pop();
$a->top();
