﻿<?php

error_reporting(-1);

mb_internal_encoding('utf-8');

echo "<h3>Пример 5.</h3>";

echo "<h3>Шифровка</h3>";

$code = array(
    'а'	=>	'1',
    'б'	=>	'2',
    'в'	=>	'3',
    'г'	=>	'4',
    'д'	=>	'5',
    'е'	=>	'6',
    'ё'	=>	'7',
    'ж'	=>	'8',
    'з'	=>	'9',
    'и'	=>	'0',
    'й'	=>	'#',
	'н'	=>	'аер6',
	'с'	=>	'5ап',
	'п'	=>	'4вп',
	'р'	=>	'кв2п',
	'л'	=>	'кв4п',
	'о'	=>	'4п'
);

$text = 'нас предали. явка провалена.';
$cipher = strtr($text, $code);

echo "<p>Оригинал: {$text}</p><p>Шифровка: {$cipher}</p>";

$fliparray = array_flip($code);
$cipher = strtr($cipher, $fliparray);

echo "Расшифровка: {$cipher}";


echo "<h3>l33tspeak - латиница</h3>";

$code = array(



    'А'	=>	'A',
    'Б'	=>	'6',
    'В'	=>	'8',
    'Г'	=>	'r',
	'Д'	=>	'D',
    'Е'	=>	'€',
    'Ж'	=>	'>|<',
    'З'	=>	'3',
	'И'	=>	'|/|',
	'Й'	=>	'U*',
	'K'	=>	'|<',
    'Л'	=>	'^',
    'М'	=>	'|\/|',
	'Н'	=>	'Н',
	'О'	=>	'0',
	'П'	=>	'||',
	'Р'	=>	'|o',
	'С'	=>	'С',
	'Т'	=>	'+',
	'У'	=>	'y',
	'Ф'	=>	'(|)',
	'Х'	=>	'X',
	'Ц'	=>	'L|',
	'Ч'	=>	'4',
	'Ш'	=>	'LLI',
	'Щ'	=>	'LLL',
	'Ъ'	=>	'~b',
	'Ы'	=>	'bI',
	'Ь'	=>	'b',
	'Ю'	=>	'IO',
	'Я'	=>	'9|',
);

$text = 'Я У МАМЫ ПИ-ЭЙЧ-ПИ ИНЖЕНЕР-ПРОГРАММИСТ';
$l33t = strtr($text, $code);

echo "Ты говоришь: {$text}.<br>А школьники говорят: {$l33t}";

echo "<h3>php - rифмоплет!</h3>";

$text = null;

$word1 = array('Чудесных', 'Суровых', 'Занятных', 'Внезапных');
$word2 = array('слов', 'зим', 'глаз', 'дней', 'лет', 'мир', 'взор');
$word3 = array('прикосновений', 'поползновений', 'судьбы явлений',
 'сухие листья', 'морщины смерти', 'долины края', 'замены нету', 
'сухая юность', 'навек исчезнув');
$word4 = array('обретаю', 'понимаю', 'начертаю', 'закрываю', 'оставляю',
 'вынимаю', 'умираю', 'замерзаю', 'выделяю');
$word5 = array('очертания', 'безысходность', 'начертанья', 'смысл жизни',
 'вирус смерти', 'радость мира');
 
 for($i=0;$i<2;$i++){
	$text1 = array_rand($word1,1);
	$text2 = array_rand($word2,1);
	$text3 = array_rand($word3,1);
	$text .= $word1[$text1].' ';
	$text .= $word2[$text2].' ';
	$text .= $word3[$text3].'<br>';
 }
 $text4 = array_rand($word4,1);
 $text5 = array_rand($word5,1);
 $text .= 'Я '.$word4[$text4].' ';
 $text .= $word5[$text5].'!';
 
 echo $text;
 
echo "<h3>Проверка на палиндром</h3>";
 
 $palindrome = "А роза упала на лапу Азора";
 
 echo "<p>Строка: {$palindrome}</p>";
 
 $palindrome = str_replace(' ','',$palindrome);
 
 $palindrome = mb_strtolower($palindrome);
 
 $letters = preg_split('//u', $palindrome, null, PREG_SPLIT_NO_EMPTY);
 
 $lenght = count($letters);
 
 $count = null;
 for($i=0;$i<$lenght/2;$i++){
	if($letters[$i] == $letters[$lenght-$i-1]){
		$count++;	 
	 }else{
		 break;		 
	 } 
 }
 
if(round($lenght/2) == $count){
	echo "Палиндром!";
}else{
	echo "Не палиндром!";
}
	 

 